*** Settings ***
Library     SeleniumLibrary

*** Variables ***
${BROWSER}   chrome
${URL}   https://teleconsult.telehealththai.com
${Username}   200085
${Password}   87654321
*** Keyword ***



** Test Cases ***
1. เปิดเว็ปไซต์ telehealththai 
    Open Browser  ${URL}  ${BROWSER}
    Maximize Browser Window
    Set Selenium Speed  0.3
2. กรอก User password
    Input Text  name=username  ${Username}
    Input Password   name=password  ${Password}
3. กด login 
    Click Element    //body[1]/section[1]/div[1]/div[1]/div[1]/form[1]/div[5]/div[1]/button[1]
4. กดเริ่มต้นใช้งาน 
    # Set Selenium Speed  0.3
    Click Element    //*[@id="loginform"]/div[3]/div/a
    # Click Element    //body[1]/section[1]/div[1]/div[1]/div[1]/form[1]/div[3]/div[1]/a[1]
5. เข้ารายการรอให้คำแนะนำ 
    Click Element    //body[1]/div[2]/div[1]/div[2]/div[1]/div[1]/div[2]/div[1]/div[1]/div[1]/a[1]
    